-- automata
local bg = '#d5d0ba'
local fg = '#45403a'

local colors = {
  color_background        = bg,
  color_foreground        = fg,
  color_cursor            = fg,
  color_cursor_foreground = bg,
  color_0 =  "#45403a",
  color_1 =  "#d70000",
  color_2 =  "#878700",
  color_3 =  "#af8700",
  color_4 =  "#0087d7",
  color_5 =  "#d70087",
  color_6 =  "#008787",
  color_7 =  "#8a8570",
  color_8 =  "#65605a",
  color_9 =  "#ff5f5f",
  color_10 = "#afaf5f",
  color_11 = "#d7af5f",
  color_12 = "#5fafff",
  color_13 = "#ff5faf",
  color_14 = "#00afaf",
  color_15 = "#d5d0ba"
}

return colors
